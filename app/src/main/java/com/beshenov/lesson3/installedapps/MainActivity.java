package com.beshenov.lesson3.installedapps;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private static final String PLAY_MARKET_URI      = "market://details?id=";
    private static final String HTTP_PLAY_MARKET_URI = "https://play.google.com/store/apps/details?id=";

    private TextView mInstalledAppTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mInstalledAppTextView = (TextView) findViewById(R.id.installedApp);

        if (getIntent() != null) {
            Intent intent = getIntent().getParcelableExtra(InstallationReceiver.BUNDLE_KEY_INTENT);
            if (intent == null) {
                return;
            }
            int uid = intent.getIntExtra(Intent.EXTRA_UID, 0);
            mInstalledAppTextView.setText(getPackageManager().getPackagesForUid(uid)[0]);
        }
    }

    public void onOpenPlayMarketClicked(View v) {
        openPlayMarket(mInstalledAppTextView.getText().toString());
    }

    private void openPlayMarket(String packageName) {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(PLAY_MARKET_URI + packageName)));
        } catch (android.content.ActivityNotFoundException exception) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(HTTP_PLAY_MARKET_URI + packageName)));
        }
    }
}
