package com.beshenov.lesson3.installedapps;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class InstallationReceiver extends BroadcastReceiver {
    public static final String BUNDLE_KEY_INTENT = "intent";

    public InstallationReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent activityIntent = new Intent(context, MainActivity.class);
        activityIntent.putExtra(BUNDLE_KEY_INTENT, intent);
        activityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(activityIntent);
    }
}
